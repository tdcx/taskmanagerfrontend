import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export function PrivateRoute({children}){
    const authState=useSelector((state)=>state.authReducer);
    const navigate=useNavigate();
    useEffect(()=>{
        if(!authState.isLoggedIn){
            navigate('/login');
        }
    });
    return children;
}