import { Box, Grid, List, ListItem } from "@mui/material";
import Skeleton from "react-loading-skeleton";
import 'react-loading-skeleton/dist/skeleton.css';
import { iconBox, taskListContainer } from "../assets/styles/TaskListStyle";

export function TaskListSkeleton() {
    return (
        <Grid item xs={12} lg={12} sx={taskListContainer}>
            <List>
                <ListItem>
                    <Grid container>
                        <Grid item xs={6}>
                            <Skeleton />
                        </Grid>
                        <Grid item xs={4}>
                            <Box sx={iconBox}>
                                <Skeleton />
                            </Box>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container>
                        <Grid item xs={6}>
                            <Skeleton />
                        </Grid>
                        <Grid item xs={4}>
                            <Box sx={iconBox}>
                                <Skeleton />
                            </Box>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container>
                        <Grid item xs={6}>
                            <Skeleton />
                        </Grid>
                        <Grid item xs={4}>
                            <Box sx={iconBox}>
                                <Skeleton />
                            </Box>
                        </Grid>
                    </Grid>
                </ListItem>
            </List>
        </Grid>
    )
}