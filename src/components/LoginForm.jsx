import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, TextField, Typography } from "@mui/material";
import { Container } from "@mui/system";
import { container, gridItem, mainGrid, textField, validationError } from "../assets/styles/CommonStyle";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { loginSchema } from "../validation-schemas/LoginSchema";
import { useDispatch, useSelector } from "react-redux";
import { userLogin } from "../actions/AuthActions";
import { useEffect, useState } from "react";
import { HttpStatusCode } from "axios";
import { useNavigate } from "react-router-dom";

export function LoginForm() {
    const [isLoading,setIsLoading]=useState(false);
    const authState=useSelector((state)=>state.authReducer);
    const [openErrorDialog, setOpenErrorDialog] = useState(false);
    const navigate=useNavigate();
    const handleOpen = () => {
        setOpenErrorDialog(true);
    };

    const handleClose = () => {
        setOpenErrorDialog(false);
    };
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(loginSchema)
    });
    const onSubmit = async (data) => {
        try {
            setIsLoading(true);
            await dispatch(userLogin(data));
            setIsLoading(false);
            navigate('/');
        } catch (error) {
            if (error.statusCode===HttpStatusCode.BadRequest) {
                setIsLoading(false);
                handleOpen();
            }
        }

    }
    useEffect(()=>{
        if(authState.isLoggedIn){
            navigate('/');
        }
    });
    return (
        <>
            <Container maxWidth={false} sx={container}>
                <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center" sx={mainGrid}>
                    <Grid item xs={3} sx={gridItem}>
                        <h3>Login</h3>
                        <TextField variant="filled" id="userId" name="userId" {...register('userId')} error={errors.userId ? true : false} label="Id" sx={textField}></TextField>
                        <Typography variant="inherit" color="red" sx={validationError}>
                            {errors.userId?.message}
                        </Typography>
                        <br />
                        <TextField variant="filled" id="name" name="name" {...register('name')} error={errors.name ? true : false} label="Name" sx={textField}></TextField>
                        <Typography variant="inherit" color="red" sx={validationError}>
                            {errors.name?.message}
                        </Typography>
                        <br />
                        <Button variant="contained" fullWidth onClick={handleSubmit(onSubmit)}>Login</Button>
                        {
                            isLoading? <CircularProgress sx={{mt:2}}></CircularProgress>:null
                        }
                        
                    </Grid>
                </Grid>
            </Container>
            <Dialog open={openErrorDialog} onClose={handleClose}>
                <DialogTitle>Login Failed</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        Please check credentials. Invalid id or name !
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Close</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}