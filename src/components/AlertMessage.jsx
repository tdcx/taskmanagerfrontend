import { Snackbar } from '@mui/material';
import MuiAlert from '@mui/material/Alert';
import React from 'react';

const CustomAlert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export function AlertMessage(props){
    return (
        <Snackbar open={props.open} autoHideDuration={6000} onClose={props.onClose} anchorOrigin={{ vertical:'bottom', horizontal:'center' }}>
                <CustomAlert onClose={props.onClose} severity={props.severity} sx={{ width: '100%' }}>
                    {props.message}
                </CustomAlert>
        </Snackbar>
    );
}
