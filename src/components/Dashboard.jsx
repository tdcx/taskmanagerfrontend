import { Button, Container, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDashboard } from "../actions/DashboardActions";
import { getAllTasks, saveTask } from "../actions/TaskActions";
import { container, gridItem, mainGrid, textField } from "../assets/styles/CommonStyle";
import { TaskList } from "./TaskList";
import { TasksSummary } from "./TasksSummary";
import { AlertMessage } from "./AlertMessage";
import { TaskFormDialog } from "./TaskFormDialog";

export function Dashboard() {
    const dashboardData = useSelector((state) => state.dashboardReducer);
    const dispatch = useDispatch();
    const [isFetched, setIsFetched] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [openSuccessDialog,setOpenSuccessDialog]=useState(false);

    useEffect(() => {
        dispatch(getAllTasks()).then(() => {
            return dispatch(getDashboard());
        }).then(() => {
            setIsFetched(true);
        });
    }, []);

    const [openTaskDialog, setOpenTaskDialog] = useState(false);

    const handleOpen = () => {
        setOpenTaskDialog(true);
    }

    const handleClose = () => {
        setOpenTaskDialog(false);
    }

    const handleAddTaskClick = async (data) => {
        setIsLoading(true);
        dispatch(saveTask(data)).then(() => { setIsLoading(false); dispatch(getDashboard()); handleClose(); openSuccessAlert() });
    }

    const openSuccessAlert = ()=>{
        setOpenSuccessDialog(true);
    }
    const closeSuccessAlert = ()=>{
        setOpenSuccessDialog(false);
    }
    return (
        <>

            <Container maxWidth={false} sx={container}>
                {
                    isFetched && dashboardData.totalTasks == 0 ?
                        <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center" sx={mainGrid}>
                            <Grid item xs={3} sx={gridItem}>
                                <h3>You have no task</h3>
                                <Button variant="contained" onClick={handleOpen}>+ New Task</Button>
                            </Grid>
                        </Grid> :
                        <Container>
                            <TasksSummary />
                            <TaskList openDialog={handleOpen} />
                        </Container>
                }


            </Container>

            <TaskFormDialog open={openTaskDialog} onClose={handleClose} title="+ New Task" isLoading={isLoading} buttonText="+ New Task" onSubmit={handleAddTaskClick} data="" />
            
            <AlertMessage open={openSuccessDialog} onClose={closeSuccessAlert} severity="success" message="New task added successfully !"/>
            
        </>
    );
}