import { Box, Container, Grid, Typography } from "@mui/material";
import { completedTasks, pieChartBox, pieChartContainer, summaryBox, summaryContainer, totalTasks } from "../assets/styles/TaskSummaryStyle";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getDashboard } from "../actions/DashboardActions";
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import { Chart } from "react-google-charts";

export function TasksSummary() {

    const dashboardData = useSelector((state) => state.dashboardReducer);
    const [isFetched, setIsFetched] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDashboard()).then(() => {
            setIsFetched(true);
        });

    }, []);
    // const data = [
    //     { title: 'Completed Tasks', value: dashboardData.tasksCompleted, color: '#1976D2' },
    //     { title: 'Pending Tasks', value: dashboardData.totalTasks - dashboardData.tasksCompleted, color: '#E0E0E0' }
    // ]
    const data = [
        ["Task Type", "Number of tasks"],
        ["Completed", dashboardData.tasksCompleted],
        ["Pending", dashboardData.totalTasks - dashboardData.tasksCompleted],
    ];
    const options = {
        legend: "none",
        pieSliceText: "label",
        title: "",
        pieStartAngle: 100,
        chartArea:{height:"80%",width: "90%"}
    };
    return (

        <>
            <Container sx={summaryContainer}>
                <Grid container rowSpacing={6} columnSpacing={4}>
                    <Grid item xs={12} lg={4}>
                        <Box sx={summaryBox}>
                            <Typography variant="p" component="p">
                                Tasks Completed
                            </Typography>
                            {isFetched ?
                                <>
                                    <Typography variant="span" component="span" sx={completedTasks}>
                                        {dashboardData.tasksCompleted}
                                    </Typography>
                                    <Typography variant="span" component="span" sx={totalTasks}>
                                        /{dashboardData.totalTasks}
                                    </Typography>
                                </>
                                :
                                <>
                                    <Typography variant="span" component="span" sx={completedTasks}>
                                        <Skeleton />
                                    </Typography>
                                </>}


                        </Box>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Box sx={summaryBox}>
                            <Typography variant="p" component="p">
                                Latest Created Tasks
                            </Typography>
                            {
                                isFetched ? <>
                                    <ul>
                                        {
                                            dashboardData.latestTasks.map((task, index) => {
                                                return (
                                                    <li key={`task-${index}`} style={task.isCompleted ? { textDecoration: 'line-through' } : null}>{task.name}</li>
                                                )
                                            })
                                        }
                                    </ul>
                                </> :
                                    <>
                                        <Skeleton count={3}></Skeleton>
                                    </>
                            }

                        </Box>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Box sx={pieChartBox}>
                            <Box sx={pieChartContainer}>
                                {
                                    isFetched ? <>
                                        <Chart
                                            chartType="PieChart"
                                            data={data}
                                            options={options}
                                        />
                                    </> :
                                        <>
                                            <Skeleton circle={true} width='37%' height={100}></Skeleton>
                                        </>
                                }

                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </>

    );
}