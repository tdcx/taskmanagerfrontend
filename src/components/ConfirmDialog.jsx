import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, LinearProgress } from "@mui/material";

export function ConfirmDialog(props) {
    return (
        <Dialog open={props.open} onClose={props.onClose}>
            {props.isLoading ? <LinearProgress></LinearProgress> : null}
            <DialogTitle>Confirmation</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {props.children}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onYesClick} size="small" variant="contained" autoFocus color="success">Yes</Button>
                <Button onClick={props.onNoClick} size="small" variant="contained" color="error">No</Button>
            </DialogActions>
        </Dialog>
    );
}