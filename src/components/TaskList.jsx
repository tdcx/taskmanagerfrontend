import { Container, Grid, List, Typography } from "@mui/material";
import { taskListContainer} from "../assets/styles/TaskListStyle";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getAllTasks, getTasksByName, removeTask, updateTask } from "../actions/TaskActions";
import { getDashboard } from "../actions/DashboardActions";
import 'react-loading-skeleton/dist/skeleton.css';
import { AlertMessage } from "./AlertMessage";
import { ConfirmDialog } from "./ConfirmDialog";
import { TaskFormDialog } from "./TaskFormDialog";
import { TaskListSkeleton } from "./TaskListSkeleton";
import { TaskListItem } from "./TaskListItem";
import { TaskListHeader } from "./TaskListHeader";

export function TaskList(props) {

    const tasks = useSelector(state => state.taskReducer);
    const [isFetched, setIsFetched] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [openDeleteSuccessDialog,setOpenDeleteSuccessDialog]=useState(false);
    const [openEditSuccessDialog,setOpenEditSuccessDialog]=useState(false);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllTasks()).then(() => {
            setIsFetched(true)
        });
    }, []);

    
    const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
    const [openEditDialog, setOpenEditDialog] = useState(false);
    const [selectedTask, setSelectedTask] = useState({});

    const handleDeleteIconClick = (task) => () => {
        setSelectedTask(task);
        setOpenConfirmDialog(true);
    }
    const closeConfirmDialog = () => {
        setOpenConfirmDialog(false);
    }
    const handleDeleteTask = () => {
        setIsLoading(true);
        dispatch(removeTask(selectedTask._id)).then(() => {setIsLoading(false); dispatch(getDashboard()); closeConfirmDialog(); openDeleteSuccessAlert() });
        
    }
    const handleEditIconClick = (task) => () => {
        setSelectedTask(task);
        setOpenEditDialog(true);
    }
    const closeEditDialog = () => {
        setOpenEditDialog(false);
    }
    
    const onUpdateSubmit = async (data) => {
        setIsLoading(true);
        dispatch(updateTask(selectedTask._id, data)).then(() => {setIsLoading(false); dispatch(getDashboard());closeEditDialog(); openEditSuccessAlert() });

    }
    const handleCheckboxChange = (task) => (e) => {
        dispatch(updateTask(task._id, { isCompleted: e.target.checked })).then(() => { dispatch(getDashboard()) })
    }
    const handleSearchByName = (e) => {
        e.target.value.length === 0 ? dispatch(getAllTasks()) : dispatch(getTasksByName(e.target.value))
    }
    const openDeleteSuccessAlert = ()=>{
        setOpenDeleteSuccessDialog(true);
    }
    const closeDeleteSuccessAlert = ()=>{
        setOpenDeleteSuccessDialog(false);
    }
    const openEditSuccessAlert = ()=>{
        setOpenEditSuccessDialog(true);
    }
    const closeEditSuccessAlert = ()=>{
        setOpenEditSuccessDialog(false);
    }
    return (
        <>
            <TaskListHeader onTextFieldChange={handleSearchByName} onButtonClick={props.openDialog} />
            {/* Tasks List */}
            <Container>
                {
                    isFetched ? <>
                        {
                            tasks.length === 0 ? <Typography variant="h6" align="center">No task found by given name</Typography> : <Grid container>
                                <Grid item xs={12} lg={12} sx={taskListContainer}>

                                    <List>
                                        {
                                            tasks.map((task) => {
                                                return (
                                                    <TaskListItem key={`item-${task._id}`} task={task} onCheckBoxChange={handleCheckboxChange(task)} onEditClick={handleEditIconClick(task)} onDeleteClick={handleDeleteIconClick(task)} />
                                                )
                                            })
                                        }
                                    </List>
                                </Grid>
                            </Grid>
                        }
                    </> : <TaskListSkeleton/>
                }
            </Container>
            {/* Tasks List Ends */}

            {/* Confirm Dialog */}
            <ConfirmDialog open={openConfirmDialog} onClose={closeConfirmDialog} isLoading={isLoading} onYesClick={handleDeleteTask} onNoClick={closeConfirmDialog} >
                <Typography>Are you sure you want to delete the following task?</Typography>
                <Typography>{selectedTask.name}</Typography>
            </ConfirmDialog>
            {/* Confirm Dialog ends */}

            {/* Edit Dialog */}
            <TaskFormDialog open={openEditDialog} onClose={closeEditDialog} title="Edit task" isLoading={isLoading} buttonText="Update Task" onSubmit={onUpdateSubmit} data={selectedTask} />
            {/* Edit Dialog ends */}

            <AlertMessage open={openDeleteSuccessDialog} onClose={closeDeleteSuccessAlert} severity="success" message={`${selectedTask.name} deleted successfully !`} />
            <AlertMessage open={openEditSuccessDialog} onClose={closeEditSuccessAlert} severity="success" message="Task updated successfully !" />
        </>
    );
}