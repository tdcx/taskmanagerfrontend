import { Button, Container, Grid, InputAdornment, TextField, Typography } from "@mui/material";
import { mainContainer, newTaskButton, searchIcon } from "../assets/styles/TaskListStyle";
import SearchIcon from '@mui/icons-material/Search';

export function TaskListHeader(props) {
    return (
        <Container sx={mainContainer}>
            <Grid container>
                <Grid item xs={12} lg={4}>
                    <Typography variant="h6" component="h6">Tasks</Typography>
                </Grid>
                <Grid item xs={12} lg={8}>
                    <Grid container spacing={3} justifyContent={{ lg: "end" }}>
                        <Grid item xs={12} lg={3}>
                            <TextField
                                id="input-with-icon-textfield"
                                label="Search by task name"
                                size="small"
                                fullWidth
                                onChange={props.onTextFieldChange}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <SearchIcon sx={searchIcon} />
                                        </InputAdornment>
                                    ),
                                }}
                                variant="filled"
                            />
                        </Grid>
                        <Grid item xs={12} lg={3}>
                            <Button variant="contained" sx={newTaskButton} onClick={()=>{props.onButtonClick()}}>+ New Task</Button>
                        </Grid>
                    </Grid>


                </Grid>
            </Grid>


        </Container>
    );
}