import { Button, Dialog, DialogContent, DialogTitle, LinearProgress, TextField, Typography } from "@mui/material";
import { textField, validationError } from "../assets/styles/CommonStyle";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { taskSchema } from "../validation-schemas/TaskSchema";
import { useEffect } from "react";

export function TaskFormDialog(props) {

    const { register, handleSubmit, formState: { errors }, control, reset } = useForm({
        resolver: yupResolver(taskSchema)
    });

    useEffect(() => {
        reset(props.data);
    }, [props.data]);
    
    return (
        <Dialog open={props.open} fullWidth onClose={props.onClose}>
            {props.isLoading ? <LinearProgress></LinearProgress> : null}
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>
                <TextField id="name" name="name" {...register('name')} error={errors.name ? true : false} label="Task Name" variant="filled" fullWidth sx={textField} />
                <Typography variant="inherit" color="red" sx={validationError}>
                    {errors.name?.message}
                </Typography>
                <Button variant="contained" disabled={errors.name ? true : false} onClick={handleSubmit(props.onSubmit)} sx={{ mt: 1 }}>{props.buttonText}</Button>
            </DialogContent>
        </Dialog>
    );
}