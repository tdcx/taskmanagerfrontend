import { AppBar, Avatar, Box, Button, CardHeader, Toolbar, Typography } from "@mui/material";
import { Container } from "@mui/system";
import avatar from '../assets/static/images/avatar.jpg'
import { avatarBox, logoutButton, appBar, cardHeader } from "../assets/styles/AppBarStyle";
import { styled } from '@mui/material/styles';
import { useDispatch, useSelector } from "react-redux";
import { userLogout } from "../actions/AuthActions";
import { useNavigate } from "react-router-dom";

const RightAlignedDiv = styled('div')(({ theme }) => ({
    marginRight: 30,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  }));

export function ApplicationBar() {
    const authState=useSelector((state)=>state.authReducer);
    const dispatch=useDispatch();
    const navigate=useNavigate();
    const handleLogout=()=>{
        dispatch(userLogout());
        navigate('/login');
    }
    if (!authState.isLoggedIn) {
        return null;
    }
    return (
        <AppBar position="static" sx={appBar}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Box sx={avatarBox}>
                        <CardHeader avatar={<Avatar alt="Remy Sharp" src={avatar} />} title={<Typography variant="span" component='span' sx={cardHeader}>{authState.user}</Typography>}></CardHeader>
                    </Box>
                    <RightAlignedDiv>
                        <Button sx={logoutButton} onClick={handleLogout}>
                            Logout
                        </Button>
                    </RightAlignedDiv>
                </Toolbar>
            </Container>
        </AppBar>
    );
}