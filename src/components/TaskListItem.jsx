import { Box, Checkbox, Container, Divider, FormControlLabel, Grid, IconButton, ListItem, Typography } from "@mui/material";
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone';
import { completedTaskName, iconBox, iconPadding, pendingTaskName } from "../assets/styles/TaskListStyle";

export function TaskListItem(props) {
    return (
        <Container key={`container${props.task._id}`}>
            <ListItem key={props.task._id}>
                <Grid container>
                    <Grid item xs={6}>
                        <FormControlLabel
                            value="Clean the room"
                            control={<Checkbox key={`checkbox${props.task._id}`} onChange={props.onCheckBoxChange} defaultChecked={props.task.isCompleted} />}
                            label={<Typography variant="h6" key={`label${props.task._id}`} component="h6" sx={props.task.isCompleted ? completedTaskName : pendingTaskName}>{props.task.name}</Typography>}
                            labelPlacement="end"
                            key={`formcontrol${props.task._id}`}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Box sx={iconBox} key={`iconBox${props.task._id}`}>
                            <IconButton key={`editbtn${props.task._id}`} onClick={props.onEditClick}><EditTwoToneIcon sx={iconPadding} key={`edit${props.task._id}`} /></IconButton>
                            <IconButton disabled={props.task.isCompleted} key={`deletebtn${props.task._id}`} onClick={props.onDeleteClick}><DeleteTwoToneIcon key={`delete${props.task._id}`} /></IconButton>

                        </Box>
                    </Grid>
                </Grid>



            </ListItem>
            <Divider variant="middle" component="li" key={`divider${props.task._id}`} />

        </Container>
    )
}