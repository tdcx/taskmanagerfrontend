export const BASE_URL='https://my-task-manager-k1g0.onrender.com';
export const TASKS_URL_PREFIX=`${BASE_URL}/tasks`;
export const USERS_URL_PREFIX=`${BASE_URL}/users`;