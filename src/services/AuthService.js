import axios, { HttpStatusCode } from "axios";
import { USERS_URL_PREFIX } from "../constants/ApiServiceConstants";

export async function login(credentials){
    try {
        const response = await axios.post(`${USERS_URL_PREFIX}/login`, credentials);
        if (response.status === HttpStatusCode.Ok) {
            localStorage.setItem('token', JSON.stringify(response.data));
            return Promise.resolve(response.data)
        } 
    } catch (error) {
        return Promise.reject({statusCode:error.response.status,data:error.response.data});
    }
}

export function logout(){
    localStorage.removeItem('token');
}