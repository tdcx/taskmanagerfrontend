import axios from "axios";
import { TASKS_URL_PREFIX } from "../constants/ApiServiceConstants";
import { authHeader } from "./AuthHeader";

export function save(task){
    return axios.post(TASKS_URL_PREFIX,task,{ headers: authHeader() });
}

export function getAll(){
    return axios.get(TASKS_URL_PREFIX,{ headers: authHeader() });
}

export function getDashboardData(){
    return axios.get(`${TASKS_URL_PREFIX}/dashboard`,{ headers: authHeader() });
}

export function update(id,task){
    return axios.put(`${TASKS_URL_PREFIX}/${id}`,task,{ headers: authHeader() });
}

export function remove(id){
    return axios.delete(`${TASKS_URL_PREFIX}/${id}`,{ headers: authHeader() });
}