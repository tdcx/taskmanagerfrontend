export function authHeader(){
    const data=JSON.parse(localStorage.getItem('token'));
    return data.token? { Authorization: 'Bearer ' + data.token } : {}; 
}