import * as Yup from 'yup';

export const loginSchema=Yup.object().shape({
    userId:Yup.string().required("User id is required"),
    name: Yup.string().required('Name is required')
});