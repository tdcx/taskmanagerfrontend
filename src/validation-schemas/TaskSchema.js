import * as Yup from 'yup';

export const taskSchema=Yup.object().shape({
    name: Yup.string().max(20,"Max 55 chars allowed").matches(/^(\w+\s?)*\s*$/,'Only single space is allowed').required('Name is required')
});