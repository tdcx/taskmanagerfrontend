import { useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ApplicationBar } from './components/ApplicationBar';
import { Dashboard } from './components/Dashboard';
import { LoginForm } from './components/LoginForm';
import { PrivateRoute } from './components/PrivateRoute';

function App() {
  useEffect(()=>{
    document.body.style.background = '#F5F5F5';
  },[]);
  return (
    <>
      <BrowserRouter>
        <ApplicationBar />
        <Routes>
          <Route path='/login' element={<LoginForm />}></Route>
          <Route path='/' element={<PrivateRoute><Dashboard /></PrivateRoute>}></Route>
        </Routes>
      </BrowserRouter>
    </>

  );
}

export default App;
