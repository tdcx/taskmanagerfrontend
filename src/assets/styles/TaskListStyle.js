export const taskListContainer={ borderRadius:3,background: 'white', pl: 3, mb:7 }
export const pendingTaskName={color:'#1976D2',fontSize:'1.5vmax'}
export const mainContainer={ mt: 5, mb: 2 }
export const searchIcon={ mt: 1 }
export const newTaskButton={ mt: 0.5 }
export const iconBox={ position: 'absolute', right: '3rem' }
export const iconPadding={  }
export const completedTaskName={color:'black',fontSize:'1.5vmax',textDecoration:'line-through'}