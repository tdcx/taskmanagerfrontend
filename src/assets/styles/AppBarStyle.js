export const appBar={
    //justifyContent: "space-between"
    bgcolor: "white" 
}
export const avatarBox={ ml:10,flexGrow: 1, display: { sm: 'block' }}
export const logoutButton={ color: 'black', mr: 10 }
export const cardHeader={color:'black',textTransform:'capitalize'}