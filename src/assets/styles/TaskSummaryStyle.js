export const summaryContainer={ pt:7 };
export const summaryBox={background:'white',borderRadius:3,pt:5.7,pb:5.7,pl:3,pr:3,boxShadow: 2}
export const pieChartBox={background:'white',borderRadius:3,pt:1,pb:1,boxShadow: 2}
export const pieChartContainer={ textAlign: 'center', alignItems: 'center', justifyContent: 'center' }
export const completedTasks={ fontSize: '5vw', color: '#1E88E5' }
export const totalTasks={ fontSize: '2vw' }