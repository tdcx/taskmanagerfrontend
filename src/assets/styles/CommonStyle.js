export const container={mt:0.5}
export const mainGrid={ minHeight: '100vh' }
export const gridItem={background:'white',pl:10,pr:10,pb:5,pt:2,textAlign:'center'}
export const textField={ mb: 1 }
export const validationError={textAlign:'start'}