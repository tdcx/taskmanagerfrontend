import { GET_ALL_TASKS, GET_TASKS_BY_NAME, REMOVE_TASK, SAVE_TASK, UPDATE_TASK } from "../constants/TaskActionConstants";

const initialState=[];
export const taskReducer=(tasks=initialState,action)=>{
    const { type, payload } = action;
    switch(type){
        case SAVE_TASK:return [...tasks, payload];
        case GET_ALL_TASKS:return payload;
        case UPDATE_TASK:return tasks.map(task=>task._id === payload._id?{...task,...payload}: task);
        case REMOVE_TASK:return tasks.filter(({ _id }) => _id !== payload._id);
        case GET_TASKS_BY_NAME:return tasks.filter(task => task.name.toLowerCase().includes(action.value.toLowerCase()));
        default:return tasks;
    }
}