import { combineReducers } from "redux";
import { authReducer } from "./AuthReducer";
import { dashboardReducer } from "./DashboardReducer";
import { taskReducer } from "./TaskReducer";

export const rootReducer=combineReducers({authReducer,taskReducer,dashboardReducer});