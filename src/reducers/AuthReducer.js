import { LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT } from "../constants/AuthActionConstants";

const data = JSON.parse(localStorage.getItem("token"));

const initialState = data ? { isLoggedIn: true, token:data.token, user:data.name } : { isLoggedIn: false, token: null, user:null };

export const authReducer=(state=initialState,action)=>{
    const { type, payload } = action;
    switch(type){
        case LOGIN_SUCCESS:return {...state,isLoggedIn:true,payload:payload.token,user:payload.name};
        case LOGIN_FAIL:return {...state,isLoggedIn:false,payload};
        case LOGOUT:return {...state,isLoggedIn:false,payload:null};
        default: return state;
    }
}