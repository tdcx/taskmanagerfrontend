import { GET_DASHBOARD_DATA } from "../constants/TaskActionConstants";

const initialState={}
export const dashboardReducer=(state=initialState,action)=>{
    const { type, payload } = action;
    
   
    switch(type){
        case GET_DASHBOARD_DATA:return payload;
        default: return state;
    }
}