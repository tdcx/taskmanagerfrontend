import { GET_ALL_TASKS, GET_TASKS_BY_NAME, REMOVE_TASK, SAVE_TASK, UPDATE_TASK } from "../constants/TaskActionConstants";
import { getAll, remove, save, update } from "../services/TasksService";

export const saveTask=(task)=>async(dispatch)=>{
    try {
        const response=await save(task);
        dispatch({type:SAVE_TASK,payload:response.data});
        return Promise.resolve(response.data);
    } catch (error) {
        return Promise.reject(error);   
    }
}

export const getAllTasks=()=>async(dispatch)=>{
    try {
        const response=await getAll();
        dispatch({type:GET_ALL_TASKS,payload:response.data});
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}

export const updateTask=(id,data)=>async(dispatch)=>{
    try {
        const response=await update(id,data);
        dispatch({type:UPDATE_TASK,payload:response.data});
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}

export const removeTask=(id)=>async(dispatch)=>{
    try {
        const response=await remove(id);
        dispatch({type:REMOVE_TASK,payload:response.data});
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}

export const getTasksByName=(name)=>async(dispatch)=>{
    try {
        dispatch({type:GET_TASKS_BY_NAME,value:name});
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}