import { LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT } from "../constants/AuthActionConstants";
import { login, logout } from "../services/AuthService"

export const userLogin=(credentials)=>async(dispatch)=>{
    try {
        const data=await login(credentials);
        dispatch({type:LOGIN_SUCCESS,payload:data});
        return Promise.resolve();
    } catch (error) {
        dispatch({type:LOGIN_FAIL,payload:error});
        return Promise.reject(error);
    }
}

export const userLogout = () => (dispatch) => {
    logout();
    dispatch({type: LOGOUT});
};