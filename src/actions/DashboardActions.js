import { GET_DASHBOARD_DATA } from "../constants/TaskActionConstants";
import { getDashboardData } from "../services/TasksService";

export const getDashboard=()=>async(dispatch)=>{
    try {
        const response=await getDashboardData();
        dispatch({type:GET_DASHBOARD_DATA,payload:response.data});
        return Promise.resolve();
    } catch (error) {
        return Promise.reject(error);
    }
}